import { createSignal } from "https://esm.sh/solid-js@1.8.1";
import { effect } from "https://esm.sh/solid-js@1.8.1/web";

const buttonSpeaker = document.querySelector(".js-button-speaker");
const buttonParticipant = document.querySelector(".js-button-participant");
const speakerForm = document.querySelector(".js-form-speaker");
const signupTypeInput = document.querySelector(".js-signup-type");

const [signupType, setSignupType] = createSignal("participant");

effect(() => {
  const type = signupType();
  if (type === "participant") {
    buttonSpeaker.classList.remove("active");
    buttonParticipant.classList.add("active");

    speakerForm.hidden = true;
  } else {
    buttonSpeaker.classList.add("active");
    buttonParticipant.classList.remove("active");

    speakerForm.hidden = false;
  }
});

effect(() => {
  signupTypeInput.value = signupType();
});

buttonSpeaker.addEventListener("click", () => {
  setSignupType("speaker");
});

buttonParticipant.addEventListener("click", () => {
  setSignupType("participant");
});
