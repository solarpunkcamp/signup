import type { Translations } from "..";
import type { Index, Layout, Message } from "../templates";

const layout: Layout = {
  pageTitle: "Solarpunk Camp 2024 - Anmeldung",
  language: "de",
};

const index: Index = {
  ...layout,

  introHeader: "Moin!",
  introText:
    "wir freuen uns, dass du beim Solarpunk Camp 2024 dabei bist.\n\nFür die Nutzung des Veranstaltungsorts bitten wir dich um eine Spende von 15-20 Euro pro Tag, um die Infrastrukturkosten zu decken. Die Teilnahme am Camp selber ist kostenlos.",

  generalInformation: "Allgemeine Informationen",
  name: "Name",
  email: "Email",
  signupTypeQuestion:
    "Möchtest du dich als Speaker*in an dem Inhalt des Camps beteiligen oder einfach als Teilnehmer*in vorbeischauen?",
  signupTypeSpeaker: "Speakerin",
  signupTypeParticipant: "Teilnehmerin",
  talkTitle: "Titel deines Beitrags",
  talkDescription: "Beschreibe kurz deinen Beitrag",

  mealsHeader: "Verpflegung",
  mealsIntroText:
    "Bitte gib an bei welchen Malzeiten zu teilnehmen möchtest. (auf Spendenbasis - Empfehlung 3-6 Euro pro Mahlzeit)",
  friday: "Freitag",
  saturday: "Samstag",
  sunday: "Sonntag",
  breakfast: "Frühstück",
  lunch: "Mittagessen",
  dinner: "Abendessen",

  glampingHeader: "Glamping",
  glampingIntroText:
    "Es kann auf dem Gelände mit dem eigenen Zelt gecampt werden. Zusätzlich kann ein Schlafplatz in einem Glamping Zelt mit Bett gebucht werden (19 Euro pro Nacht).",
  glampingFriday: "Glamping Schlafplatz Fri-Sat (19 Euro)",
  glampingSaturday: "Glamping Schlafplatz Sat-Sun (19 Euro)",

  submit: "Anmelden",
};

const maxCapacity: Message = {
  ...layout,
  title: "Wir sind voll",
  message:
    "Leider haben wir bereits so viele Anmeldungen erhalten, dass unser Veranstaltungsort nicht noch mehr Teilnehmende aufnehmen kann. Schau gerne später noch einmal vorbei, falls sich noch Menschen abmelden, werden wieder Plätze frei!",
};

const success: Message = {
  ...layout,
  title: "Du bist nun beim Solarpunk Camp 2024 angemeldet.",
  message:
    "Sollte doch etwas dazwischen kommen, melde dich bitte bei uns unter solarpunkcamp@proton.me.",
};

const error: Message = {
  ...layout,
  title: "Oh nein!",
  message:
    "Da ist etwas schief gelaufen. Bitte versuche es noch mal. Falls die Anmeldung weiterhin nicht klappt, melde dich bitte bei uns unter solarpunkcamp@proton.me.",
};

const german: Translations = {
  index,
  error,
  maxCapacity,
  success,
};

export default german;
