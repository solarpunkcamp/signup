import type { Translations } from "..";
import type { Index, Layout, Message } from "../templates";

const layout: Layout = {
  pageTitle: "Solarpunk Camp 2024 - Inscripción",
  language: "es",
};

const index: Index = {
  ...layout,

  introHeader: "¡Hola!",
  introText: "Nos alegra que te unas al Solarpunk Camp 2024.",

  generalInformation: "Información General",
  name: "Nombre",
  email: "Correo Electrónico",
  signupTypeQuestion:
    "¿Quieres participar como ponente en el contenido del campamento o simplemente asistir como participante?",
  signupTypeSpeaker: "Ponente",
  signupTypeParticipant: "Participante",
  talkTitle: "Título de tu Contribución",
  talkDescription: "Describe brevemente tu contribución",

  mealsHeader: "Alimentación",
  mealsIntroText:
    "Indica en qué comidas te gustaría participar. (Basado en donaciones - Recomendado 3-6 euros por comida)",
  friday: "Viernes",
  saturday: "Sábado",
  sunday: "Domingo",
  breakfast: "Desayuno",
  lunch: "Almuerzo",
  dinner: "Cena",

  glampingHeader: "Glamping",
  glampingIntroText:
    "Es posible acampar en el lugar con tu propia tienda. Además, se puede reservar un lugar para dormir en una tienda de Glamping con cama (19 euros por noche).",
  glampingFriday: "Lugar para Dormir Glamping Vie-Sáb (19 Euros)",
  glampingSaturday: "Lugar para Dormir Glamping Sáb-Dom (19 Euros)",

  submit: "Registrarse",
};

const maxCapacity: Message = {
  ...layout,
  title: "Estamos a capacidad máxima",
  message:
    "Lamentablemente, hemos recibido tantas inscripciones que nuestro lugar no puede aceptar más participantes. Si lo deseas, vuelve a intentarlo más tarde; si algunas personas cancelan, ¡pueden quedar lugares disponibles!",
};

const success: Message = {
  ...layout,
  title: "Te has registrado exitosamente para el Solarpunk Camp 2024.",
  message:
    "Si surge algún problema, por favor contáctanos en solarpunkcamp@proton.me.",
};

const error: Message = {
  ...layout,
  title: "¡Oh no!",
  message:
    "Algo salió mal. Por favor, inténtalo nuevamente. Si la inscripción aún no funciona, por favor contáctanos en solarpunkcamp@proton.me.",
};

const spanish: Translations = {
  index,
  error,
  maxCapacity,
  success,
};

export default spanish;
