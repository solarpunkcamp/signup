import type { Translations } from "..";
import type { Index, Layout, Message } from "../templates";

const layout: Layout = {
  pageTitle: "Solarpunk Camp 2024 - Registration",
  language: "en",
};

const index: Index = {
  ...layout,

  introHeader: "Hi!",
  introText: "We are excited that you are joining the Solarpunk Camp 2024.",

  generalInformation: "General Information",
  name: "Name",
  email: "Email",
  signupTypeQuestion:
    "Do you want to participate as a speaker in the camp content or just attend as a participant?",
  signupTypeSpeaker: "Speaker",
  signupTypeParticipant: "Participant",
  talkTitle: "Title of Your Contribution",
  talkDescription: "Briefly describe your contribution",

  mealsHeader: "Meals",
  mealsIntroText:
    "Please indicate which meals you would like to participate in. (Donation-based - Recommended 3-6 euros per meal)",
  friday: "Friday",
  saturday: "Saturday",
  sunday: "Sunday",
  breakfast: "Breakfast",
  lunch: "Lunch",
  dinner: "Dinner",

  glampingHeader: "Glamping",
  glampingIntroText:
    "Camping with your own tent is possible on the premises. Additionally, a sleeping place in a Glamping tent with a bed can be booked (19 euros per night).",
  glampingFriday: "Glamping Sleeping Spot Fri-Sat (19 Euros)",
  glampingSaturday: "Glamping Sleeping Spot Sat-Sun (19 Euros)",

  submit: "Register",
};

const maxCapacity: Message = {
  ...layout,
  title: "We are at full capacity",
  message:
    "Unfortunately, we have received so many registrations that our venue cannot accommodate more participants. Feel free to check back later; if people cancel, spots may become available again!",
};

const success: Message = {
  ...layout,
  title: "You are now registered for Solarpunk Camp 2024.",
  message:
    "If something comes up, please contact us at solarpunkcamp@proton.me.",
};

const error: Message = {
  ...layout,
  title: "Oh no!",
  message:
    "Something went wrong. Please try again. If the registration still doesn't work, please contact us at solarpunkcamp@proton.me.",
};

const english: Translations = {
  index,
  error,
  maxCapacity,
  success,
};

export default english;
