import mustache from "mustache";
import type { ParsedSignup } from ".";

export interface Layout {
  pageTitle: string;
  language: "de" | "en" | "es";
}

export interface Index extends Layout {
  introHeader: string;
  introText: string;

  pageTitle: string;
  generalInformation: string;
  name: string;
  email: string;
  signupTypeQuestion: string;
  signupTypeSpeaker: string;
  signupTypeParticipant: string;
  talkTitle: string;
  talkDescription: string;

  mealsHeader: string;
  mealsIntroText: string;
  friday: string;
  saturday: string;
  sunday: string;
  breakfast: string;
  lunch: string;
  dinner: string;

  glampingHeader: string;
  glampingIntroText: string;
  glampingFriday: string;
  glampingSaturday: string;

  submit: string;
}

export async function renderIndex(index: Index): Promise<Response> {
  const head = await Bun.file("./templates/head.mustache.html").text();
  const file = await Bun.file("./templates/index.mustache.html").text();
  return htmlResponse(mustache.render(file, index, { head }));
}

export interface Message extends Layout {
  title: string;
  message: string;
}

export async function renderMessage(message: Message): Promise<Response> {
  const head = await Bun.file("./templates/head.mustache.html").text();
  const file = await Bun.file("./templates/message.mustache.html").text();
  return htmlResponse(mustache.render(file, message, { head }));
}

export async function renderSignupList(
  signups: Array<ParsedSignup>
): Promise<Response> {
  const head = await Bun.file("./templates/head.mustache.html").text();
  const file = await Bun.file("./templates/list-signups.mustache.html").text();
  return htmlResponse(mustache.render(file, { signups }, { head }));
}

function htmlResponse(text: string): Response {
  const response = new Response(text);
  response.headers.set("Content-Type", "text/html");
  return response;
}
