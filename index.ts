import type { BunFile } from "bun";
import { Database } from "bun:sqlite";
import {
  renderIndex,
  renderMessage,
  type Index,
  type Message,
  renderSignupList,
} from "./templates";
import german from "./translations/german";
import english from "./translations/english";
import spanish from "./translations/spanish";

const checkboxFields: (keyof ParsedSignup)[] = [
  "fridayDinner",
  "saturdayBreakfast",
  "saturdayLunch",
  "saturdayDinner",
  "sundayBreakfast",
  "sundayLunch",
  "fridayGlamping",
  "saturdayGlamping",
];

const BASE_PATH = "./public";
const ADMIN_CREDENTIALS = process.env["ADMIN_CREDENTIALS"];

const db = new Database("db.sqlite");
db.exec("PRAGMA journal_mode = WAL;");

db.run(`
  create table if not exists signups (
    name text not null,
    email text not null,
    signupType text not null,
    talkTitle text,
    talkDescription text,
    ${checkboxFields.map((f) => `"${f}" bool not null`).join(",\n")}
  );
`);

const insertSignup = db.query(`
  insert into signups
  (
    name, email, signupType, talkTitle, talkDescription,
    ${checkboxFields.map((f) => `"${f}"`).join(", ")}
  )
  values (
    @name, @email, @signupType, @talkTitle, @talkDescription,
    ${checkboxFields.map((f) => `@${f}`).join(", ")}
  );
`);

const listSignups = db.query(`
    select * from signups;
`);

const currentNumberOfSignups = db.query<{ count: number }, []>(`
    select count(*) as count from signups;
`);

const server = Bun.serve({
  port: process.env["PORT"],
  async fetch(request) {
    const url = new URL(request.url);
    let path = url.pathname;
    let translations = getTranslations(path);

    try {
      if (request.method === "GET") {
        return get(request, path, translations);
      }

      if (request.method === "POST") {
        console.log(request.url);
        return submit(request, translations);
      }

      return await renderMessage(translations.error);
    } catch (e) {
      console.error(e);
      return await renderMessage(translations.error);
    }
  },
});

process.on("SIGTERM", () => {
  console.log("Received SIGTERM. Exiting");
  server.stop();
});

async function get(
  request: Request,
  path: string,
  translations: Translations
): Promise<Response> {
  if (["/en", "/de", "/es", "/"].includes(path)) {
    if (isFull()) {
      return renderMessage(translations.maxCapacity);
    }
    return await renderIndex(translations.index);
  }

  if ("/list-signups" === path) {
    if (
      typeof ADMIN_CREDENTIALS !== "string" ||
      ADMIN_CREDENTIALS.length === 0
    ) {
      throw new Error(
        "ADMIN_CREDENTIALS environment variable not set, can't display admin page."
      );
    }
    const correctAuthHeader = `Basic ${btoa(ADMIN_CREDENTIALS)}`;
    if (request.headers.get("Authorization") === correctAuthHeader) {
      const signups = listSignups.all();
      console.log(signups);
      return renderSignupList(signups);
    }

    return new Response("", {
      headers: { "WWW-Authenticate": "Basic realm=signups" },
      status: 401,
    });
  }

  return new Response(getResource(path));
}

async function submit(
  request: Request,
  translations: Translations
): Promise<Response> {
  if (isFull()) {
    return await renderMessage(translations.maxCapacity);
  }
  const data = await request.formData();
  console.log("Got a signup: ", data);

  const parsedSignup = parse(data);
  const params = Object.entries(parsedSignup).map(([field, value]) => [
    `@${field}`,
    value,
  ]);
  insertSignup.run(Object.fromEntries(params));

  return await renderMessage(translations.success);
}

export interface ParsedSignup {
  name: string;
  email: string;
  signupType: string;
  talkTitle: string;
  talkDescription: string;

  // Checkboxes
  fridayDinner: boolean;
  saturdayBreakfast: boolean;
  saturdayLunch: boolean;
  saturdayDinner: boolean;
  sundayBreakfast: boolean;
  sundayLunch: boolean;

  fridayGlamping: boolean;
  saturdayGlamping: boolean;
}

function parse(data: FormData): ParsedSignup {
  const name = parseRequiredString(data, "name");
  const email = parseRequiredString(data, "email");
  const signupType = parseRequiredString(data, "signupType");
  const talkTitle = parseOptionalString(data, "talkTitle");
  const talkDescription = parseOptionalString(data, "talkDescription");

  const checkboxEntries = checkboxFields.map((fieldName) => [
    fieldName,
    parseCheckbox(data, fieldName),
  ]);

  for (let entry of data.keys()) {
    throw new Error(`unknown form entry submitted: ${entry}`);
  }

  return {
    name,
    email,
    signupType,
    talkTitle,
    talkDescription,
    ...Object.fromEntries(checkboxEntries),
  };
}

function parseRequiredString(
  data: FormData,
  field: keyof ParsedSignup
): string {
  const value = data.get(field);
  data.delete(field);
  if (typeof value !== "string" || value.length === 0) {
    throw new Error(`Missing value for field: ${field}`);
  }
  return value;
}

function parseOptionalString(
  data: FormData,
  field: keyof ParsedSignup
): string | null {
  const value = data.get(field);
  data.delete(field);
  if (typeof value !== "string" || value.length === 0) {
    return null;
  }
  return value;
}

function parseCheckbox(data: FormData, field: keyof ParsedSignup): boolean {
  const value = data.get(field);
  data.delete(field);
  if (typeof value === "string" && value.length > 0) {
    return true;
  } else if (value === null) {
    return false;
  } else {
    throw new Error(`Invalid value for food field "${field}": ${value}`);
  }
}

function getResource(path: string): BunFile {
  return Bun.file(BASE_PATH + path);
}

function isFull(): boolean {
  const currentSignups = currentNumberOfSignups.get();
  if (currentSignups === null) {
    throw new Error("Couldn't fetch current number of signups");
  }

  return currentSignups["count"] >= 35;
}

export interface Translations {
  index: Index;
  error: Message;
  success: Message;
  maxCapacity: Message;
}

function getTranslations(path: string): Translations {
  const langFromPath = path.match(/^\/(en|es|de)($|\/)/);
  if (langFromPath === null) {
    return german;
  }

  if (langFromPath[1] === "en") {
    return english;
  }
  if (langFromPath[1] === "es") {
    return spanish;
  }

  return german;
}
